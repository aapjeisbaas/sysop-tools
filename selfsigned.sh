#!/bin/bash

# fast ssl cert without ca

ENDPOINT=$1

openssl genrsa -out $ENDPOINT.key 2048
openssl req -new -x509 -key $ENDPOINT.key -out $ENDPOINT.crt -days 3650 -subj /CN=$ENDPOINT
