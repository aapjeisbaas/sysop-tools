#!/bin/bash

# Site load tester
# usage:
# ./clicky.sh https://domain.tld/overview domain user:pass
# This will find urls on $1 choose a random url that matches $2 to visit and repeat this procedure
# Handle with care this generates a lot of random like traffic

HARKO=$1
CHECK=$2
AUTH=$3

while true ; do 
  NEXT=$(curl -s --max-time 1 --connect-timeout 1 -u $AUTH "$HARKO" | grep "https:\|http:" | grep "$CHECK" |sed 's/.*"htt/htt/g; s/".*//g' |grep "$CHECK"  | grep "^http" | grep -ve '#' | sort -R | head -n 1)
  HARKO=$NEXT
  if [ ${#HARKO} -lt 5 ] ; then
    HARKO=$1
  fi
  echo $HARKO
done