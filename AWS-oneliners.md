
### List all public ssh keys of iam users
This could be used with sshKeyCommand to authenticate on servers
```
for user in $(aws iam list-users | jq '.Users[] .UserName' -r) ; do id=$(aws iam list-ssh-public-keys --user-name $user | jq '.SSHPublicKeys[] .SSHPublicKeyId' -r ); if [ ${#id} -gt 3 ]; then aws iam get-ssh-public-key --user-name $user --ssh-public-key-id $id --encoding SSH | jq '.SSHPublicKey .SSHPublicKeyBody' -r ;fi; done
```
