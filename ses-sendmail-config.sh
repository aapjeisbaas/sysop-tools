#!/bin/bash
# 
# Stein van Broekhoven 11-05-2018
# 
# this script is used to connect local sendmail with ses for outbound mail.
# Config can be stored in SSM all with encryption:
# /smtp/user      AKIALOZRE474JVTJPXUR
# /smtp/password  iCWTmt19aYDGTnKV2lWoYIjBWp2cQdJNUN28qd1W
# /smtp/region    email-smtp.eu-west-1.amazonaws.com
# /smtp/domain    example.com
# 
# How to use?
# Pull this file in with user data, make it excuteable and run it.

# pull in the smtp credentials:
SMTP_USER=$(aws ssm get-parameter --name /smtp/user --with-decryption|jq -r .Parameter.Value)
SMTP_PASS=$(aws ssm get-parameter --name /smtp/password --with-decryption|jq -r .Parameter.Value)
SMTP_REGION=$(aws ssm get-parameter --name /smtp/region --with-decryption|jq -r .Parameter.Value)
SMTP_DOMAIN=$(aws ssm get-parameter --name /smtp/domain --with-decryption|jq -r .Parameter.Value)

echo 'AuthInfo:'${SMTP_REGION}' "U:root" "I:'${SMTP_USER}'" "P:'${SMTP_PASS}'" "M:LOGIN"' >> /etc/mail/authinfo
chmod 600 /etc/mail/authinfo
makemap hash /etc/mail/authinfo.db < /etc/mail/authinfo

echo "Connect:${SMTP_REGION} RELAY" >> /etc/mail/access
makemap hash /etc/mail/access.db < /etc/mail/access

sed -i 's/MAILER(smtp)dnl\|MAILER(procmail)dnl/dnl #/g' /etc/mail/sendmail.mc

cat << "EOT" >> /etc/mail/sendmail.mc
define(`confCACERT_PATH', `/etc/pki/tls/certs')dnl
define(`confCACERT', `/etc/pki/tls/certs/ca-bundle.crt')dnl
define(`SMART_HOST', `[REGION]')dnl
define(`RELAY_MAILER_ARGS', `TCP $h 587')dnl
define(`confAUTH_MECHANISMS', `LOGIN PLAIN')dnl
FEATURE(`authinfo', `hash -o /etc/mail/authinfo.db')dnl
MASQUERADE_AS(`[DOMAIN]')dnl
FEATURE(masquerade_envelope)dnl
FEATURE(masquerade_entire_domain)dnl
MAILER(smtp)dnl
MAILER(procmail)dnl
EOT

sed -i "s/\[DOMAIN\]/$SMTP_DOMAIN/g" /etc/mail/sendmail.mc
sed -i "s/\[REGION\]/$SMTP_REGION/g" /etc/mail/sendmail.mc

chmod 666 /etc/mail/sendmail.cf
m4 /etc/mail/sendmail.mc > /etc/mail/sendmail.cf

chmod 644 /etc/mail/sendmail.cf

/sbin/service sendmail restart

