# This script is perfect for on a bastion host in EC2 if you run lots of ASG's
# bash function to ssh into the newest instance in an EC2 ASG
# permission needed:
# autoscaling describe
#         ec2 describe
# depends on:
# aws-cli
# jq

# Usage:
# tag ASG with "Name": "clusterName" (it only check value not key)
# place file here on the bastion: /etc/profile.d/cluster-ssh.sh
# turn on ssh forwarding on your workstation ( -A, -o 'ForwardAgent yes' or in .ssh/config )
# Jump trough the bastion to the newest server in the asg:
# ssh bastion -t 'cluster-ssh clusterName'

# SSM, Bastion and asg node setup
# 
# SSM "static" public key:
# 
# 
# Bastion needs ssm reed and write rights
#
# Bastion UserDate:
#    #!/bin/bash
#    sleep 10
#    export EC2_AVAIL_ZONE=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
#    export AWS_DEFAULT_REGION="`echo \"$EC2_AVAIL_ZONE\" | sed -e 's:\([0-9][0-9]*\)[a-z]*\$:\\1:'`"
#    yum update -y
#    yum install -y jq
#    pip install awscli
#    curl https://gitlab.com/aapjeisbaas/sysop-tools/raw/master/cluster-ssh.sh -o /etc/profile.d/cluster-ssh.sh
#    for name in $(aws ssm describe-parameters --filters "Key=Name,Values=/publickeys" |jq -r .Parameters[].Name|cut -d '/' -f3); do                                                                                                                                      
#      useradd --groups wheel $name                                                                                                                                                                                                                                       
#      mkdir /home/$name/.ssh                                                                                                                                                                                                                                             
#      aws ssm get-parameter --name "/publickeys/$name"|jq -r .Parameter.Value >> /home/$name/.ssh/authorized_keys                                                                                                                                                        
#      ssh-keygen -t rsa -N "" -f /home/$name/.ssh/id_rsa                                                                                                                                                                                                                 
#      aws ssm put-parameter --name "/bastion/users/$name/pubkey" --type "String" --value "$(cat /home/$name/.ssh/id_rsa.pub)" --overwrite                                                                                                                                
#      chown $name: /home/$name/ -R                                                                                                                                                                                                                                       
#    done 
# 
# ASG node: /usr/local/bin/ssm_key_command.sh
# #!/bin/bash
# export AWS_DEFAULT_REGION=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq '.region' -r)
# user=$1
# aws ssm get-parameter --name "/bastion/users/$user/pubkey"|jq -r .Parameter.Value

cluster-ssh() {

  cluster=$1

  # Avoids the annoying setup of aws cli region just use the local region
  export EC2_AVAIL_ZONE=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
  export AWS_DEFAULT_REGION="`echo \"$EC2_AVAIL_ZONE\" | sed -e 's:\([0-9][0-9]*\)[a-z]*\$:\\1:'`"

  # Get Instances in ASG
  instances=$(aws autoscaling describe-auto-scaling-groups |jq '.AutoScalingGroups[] |select (.Tags[] .Value=="'$cluster'") .Instances[] .InstanceId' -r | sort -h | uniq)

  # Get the ip of the most recently booted instance
  ip=$(aws ec2 describe-instances --instance-ids $instances --query 'Reservations[*].Instances[*].[LaunchTime,PrivateIpAddress]' --output text | sort -hr |head -n 1 | awk '{print $2}')

  echo "-----------------------------------------------------------"
  echo "  starting ssh session to: $ip in cluster: $cluster"
  echo "-----------------------------------------------------------"

  # just ssh into it don't care for new keys and such
  ssh -o 'ServerAliveInterval 60' -o 'LogLevel ERROR' -o 'StrictHostKeyChecking no' -o 'UserKnownHostsFile /dev/null' $ip

}
