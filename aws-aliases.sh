#!/bin/bash

# place this file in /etc/profile.d/

# check if in amazon
dmesg_amazon=$(dmesg |grep "amazon")
if [ ${#dmesg_amazon} -gt 2 ]; then
  export EC2_AVAIL_ZONE=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
  export AWS_DEFAULT_REGION="`echo \"$EC2_AVAIL_ZONE\" | sed -e 's:\([0-9][0-9]*\)[a-z]*\$:\\1:'`"
fi

