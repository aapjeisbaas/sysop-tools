## Tools in here ##
 - bl-check  
   black list check tool `bl-check 8.8.8.8`
 - nginx-redirect-gen  
   Nginx Location redirect creation tool from csv input: `nginx-redirect-gen 18 redirects.csv`
 - screen-switcher  
   Switch some screen tabs with a backkground script: `screen-switcher monitoring`
 - create-db-user-pass-per-db  
   Create a new user for every db with random password ` create-db-user-pass-per-db root_password`

![Google Analytics](https://www.google-analytics.com/collect?v=1&tid=UA-48206675-1&cid=555&aip=1&t=event&ec=repo&ea=view&dp=gitlab%2Fsysop-tools%2FREADME.md&dt=sysop-tools)
